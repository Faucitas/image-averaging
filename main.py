from PIL import Image
import glob

fileType = 'tif'
folder = 'test-images'

imgList = glob.glob(folder + '/*.' + fileType)
first = True
avg = ''

for img in reversed(imgList):
    
    if first:
        img1 = Image.open(img)
        first = False
        counter = 0
    else:
        alpha = 1 / (counter + 1)
        img2 = Image.open(img)
        avg = Image.blend(img1, img2, alpha)
        img1 = avg
    counter += 1
    
avg.save('Blended.' + fileType) 



